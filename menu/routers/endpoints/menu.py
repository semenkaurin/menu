from fastapi import APIRouter, Depends, status
from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID

from core.orm import create_db_session
from core.repositories.base import Repository
from core.repositories.enums import SQLOperators
from menu.models.dish import DishModel
from menu.models.menu import MenuModel
from menu.models.submenu import SubmenuModel
from menu.schemas.menu import MenuCreateSchema, MenuReadSchema, MenuUpdateSchema
from menu.services.menu import MenuService


router = APIRouter()


@router.post(
    '',
    response_model=MenuReadSchema,
    description='Эндопоинт для создания меню'
)
async def create_menu(
    request_body: MenuCreateSchema,
    db_session: AsyncSession = Depends(create_db_session),
) -> MenuModel:
    return await MenuService(db_session).create(request_body)


@router.get(
    '/{menu_id}',
    response_model=MenuReadSchema,
    description='Эндпоинт для получения данных меню'
)
async def get_menu(
    menu_id: UUID,
    db_session: AsyncSession = Depends(create_db_session),
):
    return await MenuService(db_session).get_by_id(menu_id)


@router.patch(
    '/{menu_id}',
    response_model=MenuReadSchema,
    description='Эндпоинт для обновления данных меню',
)
async def update_menu(
    menu_id: UUID,
    request_body: MenuUpdateSchema,
    db_session: AsyncSession = Depends(create_db_session),
) -> MenuModel:
    return await MenuService(db_session).update_by_id(
        menu_id=menu_id,
        request_body=request_body,
    )


@router.delete(
    '/{menu_id}',
    description='Эндопоинт для удаления меню',
)
async def delete_menu(
    menu_id: UUID,
    db_session: AsyncSession = Depends(create_db_session),
) -> None:
    return await MenuService(db_session).delete_by_id(menu_id)
