from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID

from core.orm import create_db_session
from menu.models.submenu import SubmenuModel
from menu.models.dish import DishModel
from menu.schemas.submenu import SubmenuCreateSchema, SubmenuReadSchema, SubmenuUpdateSchema
from menu.services.submenu import SubmenuService


router = APIRouter()


@router.post(
    '/{menu_id}/submenus',
    response_model=SubmenuReadSchema,
    description='Эндопнит для создания подменю',
)
async def create_submenu(
    request_body: SubmenuCreateSchema,
    menu_id: UUID,
    db_session: AsyncSession = Depends(create_db_session),
) -> SubmenuModel:
    return await SubmenuService(db_session).create(
        request_body=request_body,
        menu_id=menu_id,
    )


@router.get(
    '/{menu_id}/submenus/{submenu_id}',
    response_model=SubmenuReadSchema,
    description='Эндпоинт для получения данных подменю',
)
async def get_submenu(
    menu_id: UUID,
    submenu_id: UUID,
    db_session: AsyncSession = Depends(create_db_session),
) -> DishModel | int:
    return await SubmenuService(db_session).get_by_ids(
        menu_id=menu_id,
        submenu_id=submenu_id,
    )


@router.patch(
    '/{menu_id}/submenus/{submenu_id}',
    response_model=SubmenuReadSchema,
    description='Эндпоинт для обновления данных подменю',
)
async def update_submenus(
    menu_id: UUID,
    submenu_id: UUID,
    request_body: SubmenuUpdateSchema,
    db_session: AsyncSession = Depends(create_db_session),
) -> SubmenuModel:
    return await SubmenuService(db_session).update_by_ids(
        menu_id=menu_id,
        submenu_id=submenu_id,
        request_body=request_body,
    )


@router.delete(
    '/{menu_id}/submenus/{submenu_id}',
    description='Эндпоинт для удаления данных подменю'
)
async def delete_submenu(
    menu_id: UUID,
    submenu_id: UUID,
    db_session: AsyncSession = Depends(create_db_session),
) -> None:
    return await SubmenuService(db_session).delete_by_ids(
        menu_id=menu_id,
        submenu_id=submenu_id,
    )
