from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID

from core.orm import create_db_session
from menu.models.dish import DishModel
from menu.schemas.dish import DishCreateSchema, DishReadSchema, DishUpdateSchema
from menu.services.dish import DishService


router = APIRouter()


@router.post(
    '/{submenu_id}/dishes',
    response_model=DishReadSchema,
    description='Эндопнит для создания блюда',
)
async def create_dish(
    submenu_id: UUID,
    request_body: DishCreateSchema,
    db_session: AsyncSession = Depends(create_db_session),
) -> DishModel:
    return await DishService(db_session).create(
        submenu_id=submenu_id,
        request_body=request_body,
    )


@router.get(
    '/{submenu_id}/dishes/{dish_id}',
    response_model=DishReadSchema,
    description='Эндпоинта для получения данных блюда',
)
async def read_dish(
    dish_id: UUID,
    submenu_id: UUID,
    db_session: AsyncSession = Depends(create_db_session),
) -> DishModel:
    return await DishService(db_session).get_by_ids(
        dish_id=dish_id,
        submenu_id=submenu_id,
    )


@router.patch(
    '/{submenu_id}/dishes/{dish_id}',
    response_model=DishReadSchema,
    description='Эндпоинта для обновления данных блюда',
)
async def update_dish(
    dish_id: UUID,
    submenu_id: UUID,
    response_body: DishUpdateSchema,
    db_session: AsyncSession = Depends(create_db_session),
) -> DishModel:
    return await DishService(db_session).update_by_ids(
        dish_id=dish_id,
        submenu_id=submenu_id,
        response_body=response_body,
    )


@router.delete(
    '/{submenu_id}/dishes/{dish_id}',
    description='Эндпоинта для удаления блюда',
)
async def delete_dish(
    dish_id: UUID,
    submenu_id: UUID,
    db_session: AsyncSession = Depends(create_db_session),
) -> None:
    return await DishService(db_session).delete_by_ids(
        dish_id=dish_id,
        submenu_id=submenu_id,
    )