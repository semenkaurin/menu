from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID

from core.repositories.base import Repository
from core.repositories.enums import SQLOperators
from menu.models.submenu import SubmenuModel
from menu.models.dish import DishModel
from menu.schemas.submenu import SubmenuCreateSchema, SubmenuUpdateSchema


class SubmenuService:
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session
        self.repository = Repository(self.db_session)

    async def create(
        self,
        *,
        request_body: SubmenuCreateSchema,
        menu_id: UUID,
    ) -> SubmenuModel:
        submenu = self.repository.add_obj_to_session(
            model=SubmenuModel,
            data={**request_body.dict(), 'menu_id': menu_id}
        )
        await self.db_session.commit()

        return submenu

    async def get_by_ids(
        self,
        *,
        menu_id: UUID,
        submenu_id: UUID,
    ) -> DishModel:
        submenu = await self.repository.select_one(
            model=SubmenuModel,
            conditions=[
                ('id', SQLOperators.EQ, submenu_id),
                ('menu_id', SQLOperators.EQ, menu_id),
            ],
        )

        submenu.amount_of_dishes = await self.repository.select_count(
            model=DishModel,
            conditions=[
                ('submenu_id', SQLOperators.EQ, submenu_id),
            ],
        )

        return submenu

    async def update_by_ids(
        self,
        *,
        menu_id: UUID,
        submenu_id: UUID,
        request_body: SubmenuUpdateSchema,
    ) -> SubmenuModel:
        submenu = await self.repository.update_and_return_one(
            model=SubmenuModel,
            data=request_body.dict(),
            conditions=(
                ('id', SQLOperators.EQ, submenu_id),
                ('menu_id', SQLOperators.EQ, menu_id),
            ),
        )
        await self.db_session.commit()

        return submenu

    async def delete_by_ids(
        self,
        *,
        menu_id: UUID,
        submenu_id: UUID,
    ) -> None:
        await self.repository.delete(
            model=SubmenuModel,
            conditions=(
                ('id', SQLOperators.EQ, submenu_id),
                ('menu_id', SQLOperators.EQ, menu_id),
            ),
        )
        await self.db_session.commit()
