from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID

from core.repositories.base import Repository
from core.repositories.enums import SQLOperators
from menu.models.dish import DishModel
from menu.models.menu import MenuModel
from menu.models.submenu import SubmenuModel
from menu.schemas.menu import MenuCreateSchema, MenuUpdateSchema


class MenuService:
    '''Сервис с бизнес-логикой для меню'''
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session
        self.repository = Repository(self.db_session)

    async def create(
        self,
        request_body: MenuCreateSchema,
    ) -> MenuModel:
        menu = self.repository.add_obj_to_session(model=MenuModel, data=request_body.dict())
        await self.db_session.commit()

        return menu

    async def get_by_id(
        self,
        menu_id: UUID,
    ):
        menu = await self.repository.select_one(
            model=MenuModel,
            conditions=(
                ('id', SQLOperators.EQ, menu_id),
            ),
        )

        menu.submenus_amount = await self.repository.select_count(
            model=SubmenuModel,
            conditions=[
                ('menu_id', SQLOperators.EQ, menu_id),
            ],
        )

        submenus_in_menu = await self.repository.select_list(
            model=SubmenuModel,
            conditions=[
                ('menu_id', SQLOperators.EQ, menu_id),
            ],
        )

        amount_of_dishes = 0
        for submenu in submenus_in_menu:
            amount_of_dishes += await self.repository.select_count(
                model=DishModel,
                conditions=[
                    ('submenu_id', SQLOperators.EQ, submenu.id),
                ],
            )
        menu.dishes_amount = amount_of_dishes

        return menu

    async def update_by_id(
        self,
        *,
        menu_id: UUID,
        request_body: MenuUpdateSchema,
    ) -> MenuModel:
        menu = await self.repository.update_and_return_one(
            model=MenuModel,
            data=request_body.dict(exclude_unset=True),
            conditions=(
                ('id', SQLOperators.EQ, menu_id),
            ),
        )
        await self.db_session.commit()

        return menu

    async def delete_by_id(
        self,
        menu_id: UUID,
    ) -> None:
        await self.repository.delete(
            model=MenuModel,
            conditions=[
                ('id', SQLOperators.EQ, menu_id),
            ],
        )
        await self.db_session.commit()
