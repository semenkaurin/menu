from sqlalchemy.ext.asyncio import AsyncSession
from uuid import UUID

from core.repositories.base import Repository
from core.repositories.enums import SQLOperators
from menu.models.dish import DishModel
from menu.schemas.dish import DishCreateSchema, DishUpdateSchema


class DishService:
    '''Сервис с бизнес-логикой для блюд'''
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session
        self.repository = Repository(self.db_session)

    async def create(
        self,
        *,
        submenu_id: UUID,
        request_body: DishCreateSchema,
    ) -> DishModel:
        dish = self.repository.add_obj_to_session(
            model=DishModel,
            data={**request_body.dict(), 'submenu_id': submenu_id},
        )
        await self.db_session.commit()

        return dish

    async def get_by_ids(
        self,
        *,
        dish_id: UUID,
        submenu_id: UUID,
    ) -> DishModel:
        return await self.repository.select_one(
            model=DishModel,
            conditions=(
                ('id', SQLOperators.EQ, dish_id),
                ('submenu_id', SQLOperators.EQ, submenu_id),
            ),
        )

    async def update_by_ids(
        self,
        *,
        dish_id: UUID,
        submenu_id: UUID,
        response_body: DishUpdateSchema,
    ) -> DishModel:
        dish = await self.repository.update_and_return_one(
            model=DishModel,
            data=response_body.dict(exclude_unset=True),
            conditions=(
                ('id', SQLOperators.EQ, dish_id),
                ('submenu_id', SQLOperators.EQ, submenu_id),
            ),
        )
        await self.db_session.commit()

        return dish

    async def delete_by_ids(
        self,
        *,
        dish_id: UUID,
        submenu_id: UUID,
    ) -> None:
        await self.repository.delete(
            model=DishModel,
            conditions=(
                ('id', SQLOperators.EQ, dish_id),
                ('submenu_id', SQLOperators.EQ, submenu_id),
            ),
        )
        await self.db_session.commit()
